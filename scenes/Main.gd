extends Control
#------------------------------------------MENU TEXT & CONTROL------------------------------------------------------------------
@onready var title_label = $MarginContainer/GridContainer/Title
@onready var menu_label = $MarginContainer/GridContainer/Menu
@onready var line_edit = $MarginContainer/GridContainer/UserInput
var menu_title = "MAIN MENU"
var menu_string = "1.  Enter new trade (not functional)
	2.  View and edit strategies
	3.  View and edit instruments
	4.  Review statistics (not functional)
	5.  Save and exit (not functional, just quits.)"


# Called when the node enters the scene tree for the first time.
func _ready():
	title_label.text = menu_title
	menu_label.text = menu_string
	line_edit.grab_focus() #able to enter text instantly

# called upon LineEdit return (enter key pressed)
func _on_user_input_text_submitted(user_input):
	line_edit.clear() #Clear LineEdit after we enter something
	match user_input:
		"1":
			get_tree().change_scene_to_file("res://scenes/Trade.tscn")
		"2":
			get_tree().change_scene_to_file("res://scenes/Strategy.tscn")
		"3":
			get_tree().change_scene_to_file("res://scenes/Instrument.tscn")
		"4":
			get_tree().change_scene_to_file("res://scenes/Statistics.tscn")
		"5":
			get_tree().quit()
		_:
			menu_label.text = menu_string + "\nEnter only a displayed number to select option."





