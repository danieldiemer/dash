extends Control
#------------------------------------------MENU TEXT & CONTROL------------------------------------------------------------------
@onready var title_label = $MarginContainer/GridContainer/Title
@onready var menu_label = $MarginContainer/GridContainer/Menu
@onready var line_edit = $MarginContainer/GridContainer/UserInput
var menu_title = "STRATEGY"
var menu_string = "1.  Enter new strategy
	2.  Edit a strategy
	3.  Return to Main"

# Called when the node enters the scene tree for the first time.
func _ready():
	title_label.text = menu_title
	menu_label.text = menu_string
	line_edit.grab_focus() #able to enter text instantly


# called upon LineEdit return (enter key pressed)
func _on_user_input_text_submitted(user_input):
	line_edit.clear() #Clear LineEdit after we enter something
	match user_input:
		"1":
			pass #get_tree().change_scene_to_file()
		"2":
			pass #get_tree().change_scene_to_file()
		"3":
			get_tree().change_scene_to_file("res://scenes/Main.tscn")
		_:
			$MarginContainer/GridContainer/Menu.text = menu_string + "\nEnter only a displayed number to select option."

