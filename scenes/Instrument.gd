extends Control
#------------------------------------------MENU TEXT & CONTROL------------------------------------------------------------------
@onready var title_label = $MarginContainer/GridContainer/Title
@onready var menu_label = $MarginContainer/GridContainer/Menu
@onready var line_edit = $MarginContainer/GridContainer/UserInput
var menu_title = "INSTRUMENT"
var instruments = []
var menu_string = "1.  Enter new instrument
	2.  Edit Instrument
	3.  Return to Main"
var last_input = ""


# Called when the node enters the scene tree for the first time.
func _ready():
	title_label.text = menu_title
	menu_label.text = menu_string
	line_edit.grab_focus() #able to enter text instantly

# called upon LineEdit return (enter key pressed)
func _on_user_input_text_submitted(user_input):
	line_edit.clear() #Clear LineEdit after we enter something
	if (menu_title == "INSTRUMENT"):
		go_to_scene(user_input)
	else:
		last_input = user_input

#------------------------------------------CLASS SPECIFIC FUNCTIONS------------------------------------------------------------------
func __init__(symbol, tick_size, dollar_per_tick, RTH_open, RTH_close):
	self.symbol = symbol
	self.tick_size = tick_size
	self.dollar_per_tick = dollar_per_tick
	self.RTH_open = RTH_open
	self.RTH_close = RTH_close

func go_to_scene(user_input):
		match user_input:
			"1":
				create_new_instrument()
			"2":
				edit_instrument()
			"3":
				get_tree().change_scene_to_file("res://scenes/Main.tscn")
			_:
				menu_label.text = menu_string + "\nEnter only a displayed number to select option."
func create_new_instrument():
	##todo fill in
	await get_tree().create_timer(2.0).timeout
	get_tree().reload_current_scene()

func edit_instrument():
	#todo fill in
	menu_title = "EDIT INSTRUMENT"
	await get_tree().create_timer(2.0).timeout
	menu_label.text = "Does nothing right now, todo"
	await get_tree().create_timer(2.0).timeout
	get_tree().reload_current_scene()
	
