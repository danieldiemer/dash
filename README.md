# dash

A trading journal and trade entry planner written using the godot game engine.

## Getting started

I didn't like any of the existing trade journals, mostly because they cost upwards of $50 per month, but was frustrated with not
being able to do exactly what I wanted.  I wanted someting simple and text based so I could quickly enter information without using a mouse.

This is not a high frequency trading app, so there will be no importing of entry prices or stats via CSV after the fact as that kind of defeats the purpose of using it as a planner.

This app is meant to help plan strategies, check things off prior to entry, and compare strategy and trade metrics over
long term to say whether or not something is profitable. There are no similar applications that exist which I could find.

## Initial Features
- [ ] Instrument Setup (in progress)
- [ ] Basic strategy setup (in progress)
- [ ] Trade entry that prompts entry questions based on strategy (todo)

## Future Features
- [ ] Statistics
- [ ] Importing old database (like if backed up and installed on new computer)
